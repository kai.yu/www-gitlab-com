---
layout: job_page
title: "Content Marketing Associate"
---

GitLab is looking for a highly creative, self-motivated content marketer to help drive awareness and interest in GitLab. The Content Marketing Associate position is responsible for understanding and engaging our target audiences across a variety of channels and content formats and owning content creation from start to finish.

A successful Content Marketing Associate has strong empathy and respect for the developer experience. They are able to put themselves in the shoes of others in order to understand their needs, pain points, preferences, and ambitions and translate that into multimedia experiences. Additionally, they are excited about experimentation and analyzing the results.

## Responsibilities
- Project manage content development from start to finish
- Produce and update high-quality and engaging content including (but not limited to) blog posts, webinars, emails, white papers, web pages, and case studies
- Work with product marketing to develop a deep understanding of our product messaging and translate them into engaging narratives and multimedia experiences
- Work with internal subject matter experts and thought leaders to amplify their reach and influence
- Assist in building formalized content operations processes

## Requirements
- Excellent writer and researcher with a strong ability to grasp new concepts quickly
- Degree in marketing, journalism, communications or related field
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders
- Ability to empathize with the needs and experiences of developers
- Extremely detail-oriented and organized, able to meet deadlines
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Candidates will complete a take-home exercise writing a 300-500 word blog post from a provided prompt. Send the completed exercise to our Content Marketing Manager.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Senior Product Marketing Manager
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

