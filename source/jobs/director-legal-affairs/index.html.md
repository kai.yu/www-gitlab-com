---
layout: job_page
title: "Director of Legal Affairs"
---

The Director of Legal Affairs, a newly created position, will provide GitLab with world-class legal services that will help GitLab grow and achieve its’ business goals. The Director of Legal Affairs is responsible for providing advice and services that are strategic, timely, proactive, practical, cost-effective, and meet the highest legal, ethical, and professional standards. The Director of Legal Affairs will have responsibility for assisting with and overseeing all legal operations in the US and internationally. This is a remote role, but extensive US corporate law experience is a must-have for this role.

## Responsibilities

- Reviewing and negotiating related sales contracts with existing and prospective customers.  
- Providing strategy, guidance and deal execution for other third party agreements with resellers, in-bound technology partners and potential acquisitions.
- Helping the company design and implement its Open Source Software strategy ensuring legal compliance with inbound and outbound licensing arrangements.
- Lead the effort in defining the Company’s intellectual property strategy including consideration of patents, trademarks and copyrights.
- Coordinating with outside legal counsel in various subject matters. Ensuring the cost-effective use of outside counsel, through the use of preferred providers, technology, alternative fee arrangements, etc.
- Providing legal expertise and advice on issues arising from actual or anticipated lawsuits. Anticipating and guarding against legal risks facing GitLab.
- Coordinating with management, as appropriate, to ensure any and all contractual matters are handled in a timely, efficient, and cost-effect manner and to ensure that processes and procedures will be developed and applied consistently.
- Serving as a trusted advisor and business partner by providing expertise and guidance for the accomplishment of overall business objectives of GitLab such as fostering a culture of innovation, while ensuring full compliance with applicable laws and regulations and avoiding unacceptable risks.
- Developing a thorough understanding of and familiarity with GitLab’s business, its people, products, technology, markets, facilities, customers and competitors to identify trends and formulate strategies accordingly. Other duties as assigned.
- As a service provider to other groups within GitLab you will be measured on reducing legal review cycle times, cost reduction, strategic contributions and peer feedback.
- A typical day in the life will include reviewing customer NDA’s, defining and implementing an IP strategy, negotiating several end user license agreements, responding to employee legal issues, managing trademark issues and reviewing open source policy and processes.

## Requirements for applicant

- Law degree from a top tier US or international law school.
- Minimum 5-7 years of substantial experience in the areas of corporate law in the United States.
- Business acumen, including a well-developed understanding of business and commerce and the ability to diagnose and negotiate corporate legal issues and present positive, creative solutions and alternatives.
- Outstanding interpersonal skills, including diplomacy and flexibility, and the ability to interface effectively and engender trust and confidence with personnel at many different levels throughout GitLab.
- Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
- Previous experience in a Global Start-up and remote first environment would be ideal.
- Experience with open source software a plus.
- Experience with employment law a plus.
- Successful completion of a [background check](/handbook/people-operations/#background-checks).


## Hiring Process


Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).


* The review process for this role can take a little longer than usual but if in doubt, check in with the Global recruiter at any point
* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Candidates might at this point be invited to schedule with an additional C-Level team member or VP Management member
* Finally, candidates will interview with our CEO.
* Successful candidates will subsequently be made an offer via email.


Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
